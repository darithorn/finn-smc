module.exports = function (grunt) {

	grunt.loadNpmTasks ("grunt-mocha-test");
	grunt.loadNpmTasks ("grunt-contrib-uglify");
	
	grunt.initConfig ({
		pkg: grunt.file.readJSON ("package.json"),
		uglify: {
			finn_smc: {
				files: {
					"dist/finn.smc.min.js": [
						"src/model.js",
						"src/schema.js",
						"src/collection.js",
						"src/connection.js",
						"src/array.js",
						"src/id.js"
					]
				}
			}
		},
		mochaTest: {
			test: {
				options: {
					reporter: "spec",
				},
				src: [
					"tests/**/*.js"
				]
			}
		}
	});

	grunt.registerTask ("default", "mochaTest");
};
