var exports = exports || {};

exports.ConnectionInfo = function (hash) {
	this.url = "";

	this.__typeof__ = "connectionInfo";

	if (hash == null) {
		hash = {};
	}
	for (var key in hash) {
		this[key] = hash[key];
	}
};

exports.ConnectionInfo.prototype.makeConnection = function (options) {
	if (options == null) {
		options = {};
	}
	MongoClient.connect (this.url, function (err, db) {
		if (err) {
			// fail () is called when the connection fails
			if (options.fail != null) {
				options.fail (err);
			}
		} else {
			// success () is called when a sucessful connection has been made
			if (options.success != null) {
				options.success (db);
			}
		}
	});
};
