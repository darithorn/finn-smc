var exports = exports || {};

exports.Array = function (type) {
	return {
		type: type,
		__typeof__: "array"
	};
};
