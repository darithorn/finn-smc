var exports = exports || {};

exports.Model = function (hash) {
	this.values = {};

	this._collections = {};
	this._temp = {};

	this.__schema__ = {};
	this.__typeof__ = "model";

	if (hash == null) {
		hash = {};
	}
	for (var key in hash) {
		this[key] = hash[key];
	}
};

exports.Model.prototype.resetTemp = function () {
	this._temp = {};
};

exports.Model.prototype.addTo = function (field, model) {
	if (this._collections[field] !== undefined) {
		var collection = this._collections[field];
		if (typeof (collection) !== "object" || 
				collection.__typeof__ !== "collection") {
			throw new Error ("`%s` is not a Collection!", field);
			return;
		}
		model.values._foreign_id = this.values._id;
		if (this._temp[field] === undefined) {
			this._temp[field] = [];
		}
		this._temp[field].push (model);
	} else if (this.__schema__.types[field].__typeof__ === "array") {
		this.values[field].push (model);
	} else {
		throw new Error ("`%s` is not a container type!", field);
	}
}; 
