var exports = exports || {};
var mongo = require ("mongodb");

exports.Schema = function (hash) {
	this.types = {};
	this.defaults = {};
	this.enforceTypes = true;

	this.__typeof__ = "schema";

	for (var key in hash) {
		this[key] = hash[key];
	}
	
	if (this.enforceTypes) {
		console.warn ("** WARNING ** Enforcing types isn't implemented!");
	}
};

exports.Schema.prototype.new = function (values) {
	var temp = new exports.Model ({
		values: values,
		__schema__: this
	});
	var hasCollections = false;
	for (var key in this.defaults) {
		if (temp.values[key] == null) {
			temp.values[key] = this.defaults[key];
		}
	}
	for (var key in this.types) {
		var value = this.types[key];
		if (temp.values[key] == null) {
			if (typeof (value) === "object") {
				if (value.__typeof__ == null) {
					throw new Error ("`" + key + "` does not have a supported type!");
				} else if (value.__typeof__ === "collection") {
					hasCollections = true;
					temp._collections[key] = value;
				} else if (value.__typeof__ === "schema") {
					temp.values[key] = value.new ({});
				} else if (value.__typeof__ === "array") {
					temp.values[key] = Array ();
				}  else {
					throw new Error ("`" + key + "` does not have a supported type `" + value.__typeof__ + "`.");
				}
			} else {
				temp.values[key] = value ();
			}
		}
	}
	temp.values._id = mongo.ObjectId ();
	return temp;
};
