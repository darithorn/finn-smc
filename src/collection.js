var exports = exports || {};
var MongoClient = require ("mongodb").MongoClient;

/**
 * @callback queryCallback
 * @param {object[]} result - The result of the query
 */

/**
 * A wrapper to a MongoDB collection.
 * 
 * @name Collection
 * @class
 * @property {string} name - the name of the collection
 * @property {Schema} type - the Schema the collection contains
 * @property {ConnectionInfo} info - the ConnectionInfo used to make connections
 * @param {object} [ext] - An object that can be used to set properties and extend the Collection
 * @example
 * new Collection ({
 *     name: "myCollection",
 *     type: MySchema,
 *     info: myConnectionInfo   
 * });
 */
exports.Collection = function (ext) {
	this.name = "";
	this.type = null; // Schema
	this.info = null; // ConnectionInfo

	this.__typeof__ = "collection";

	if (ext == null) {
		ext = {};
	}
	for (var key in ext) {
		this[key] = ext[key];
	}
};

/**
 * Makes a connection to the database using the Collection's `info`
 * and inserts the `model` into the database collection.
 * Saves any temporaries the `model` has to their respective collections.
 * After saving the `model` the `model's` temporaries are reset.
 * @param {Model} model - the Model to insert into the Collection
 * @example
 * myCollection.insertOne (myModel);
 */
exports.Collection.prototype.insertOne = function (model) {
	if (this.info == null) {
		throw new Error ("No ConnectionInfo specified! Cannot make connection!");
	}
	var that = this;
	this.info.makeConnection ({
		fail: function (err) {
			throw err;
		},

		success: function (db) {
			db.collection (that.name).insertOne (model.values);
			for (var key in model._temp) {
				for (var i in model._temp[key]) {
					model._collections[key].insertOne (model._temp[key][i]);
				}
			}
			model.resetTemp ();
			db.close ();
		}
	})
};

/**
 * Makes a connection to the database using the Collection's `info`
 * and queries to find models that fit the specified `filter`.
 * If `filter` is null the query finds all models in the Collection.
 * @param {object} [filter=null] - The filter used by the query
 * @param {queryCallback} - The callback called when the query finishes
 * @example
 * // Finds the models with _id 1 and logs the result.
 * myCollection.find ({ _id: 1 }, function (result) {
 *     console.log (result);
 * });
 */
exports.Collection.prototype.find = function (filter, callback) {
	if (this.info == null) {
		throw new Error ("No ConnectionInfo specified! Cannot make connection!");
	}
	if (typeof (filter) !== "function" && typeof (callback) !== "function") {
		throw new Error ("There must be a callback to handle the result!");
	}
	if (typeof (filter) === "function") {
		callback = filter;
		filter = undefined;
	}
	var that = this;
	this.info.makeConnection ({
		fail: function (err) {
			throw err;
		},
		success: function (db) {
			var result = [];
			var cursor = db.collection (that.name).find (filter);
			cursor.each (function (err, doc) {
				if (err) {
					throw err;
				} else if (doc != null) {
					result.push (doc);
				} else {
					callback (result);
					db.close ();
				}
			});
		}
	});
};

/**
 * Makes a connection to the database using Collection's `info` and
 * deletes any models that match the filter.
 * @param {object} filter - The filter used to match the model's
 * @example
 * // Deletes any models with _id = 1
 * myCollection.deleteMany ({ _id: 1 });
 */
exports.Collection.prototype.deleteMany = function (filter) {
	if (this.info == null) {
		throw new Error ("No ConnectionInfo specified! Cannot make connection!");
	}
	var that = this;
	this.info.makeConnection ({
		fail: function (err) {
			throw err;
		},
		success: function (db) {
			db.collection (that.name).deleteMany (filter);
			db.close ();
		}
	});
};

/**
 * A shorthand function for `Collection.deleteMany ({})`
 * Deletes all of the models in the collection.
 */
exports.Collection.prototype.deleteAll = function () {
	this.deleteMany ({});
};

/**
 * Makes a connection to the database using Collection's `info`
 * and deletes the first model that matches the filter.
 * @param {object} filter - the filter used to match the model
 * @example
 * // Deletes the model with _id = 1
 * myCollection.deleteOne ({ _id : 1 });
 */
exports.Collection.prototype.deleteOne = function (filter) {
	if (this.info === null) {
		throw new Error ("No ConnectionInfo specified! Cannot make connection!");
	}
	var that = this;
	this.info.makeConnection ({
		fail: function (err) {
			throw err;
		},
		success: function (db) {
			db.collection (that.name).deleteOne (filter);
			db.close ();
		}
	});
};

/**
 * Makes a connection to the database using Collection's `info` and
 * applies the update to the models matched by the filter.
 * @param {object} filter - the filter used to match the models
 * @param {object} update - the update operation to apply to the models
 */ 
exports.Collection.prototype.updateMany = function (filter, update) {
	if (this.info == null) {
		throw new Error ("No ConnectionInfo specified! Cannot make connection!");
	}
	var that = this;
	this.info.makeConnection ({
		fail: function (err) {
			throw err;
		},
		success: function (db) {
			db.collection (that.name).updateMany (filter, update);
			db.close ();
		}
	});
};

/**
 * Makes a connection to the database using Collection's `info` and
 * applies the update to the first model matched by the filter.
 * @param {object} filter - the filter used to match the model
 * @param {object} update - the update operation to apply to the model
 */
exports.Collection.prototype.updateOne = function (filter, update) {
	if (this.info == null) {
		throw new Error ("No ConnectionInfo specified! Cannot make connection!");
	}
	var that = this;
	this.info.makeConnection ({
		fail: function (err) {
			throw err;
		},
		success: function (db) {
			db.collection (that.name).updateOne (filter, update);
			db.close ();
		}
	});
};
